/* 
 * File:   Ficheiro.h
 * Author: Pedro Gomes
 *
 * Created on 8 de Dezembro de 2014, 15:20
 */

#ifndef FICHEIRO_H
#define	FICHEIRO_H

#include <cstdlib>
#include "Armazem.h"
#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "Produto.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdio.h>

const int LINES = 2;
const int MATRIX_LINE = 100;
const string output = "Output.txt";
using namespace std;


class Ficheiro {
private:
    
    string destino;
    
public:
    Ficheiro();
    ~Ficheiro();
    Ficheiro(string &dest);
    
    string getDestino(){
        return destino;
    }
    void setDestino(string dest){
        this->destino = dest;
    }
    
    int lerDepositos(char *str, int length);
    void escreverFicheiro(const Armazem& a);
    void lerFicheiro (Armazem& a);
    
    
};
/**
 * BY DEFAULT
 */
Ficheiro::Ficheiro(){
this->destino = output;
};
Ficheiro::~Ficheiro(){};


Ficheiro::Ficheiro(string &dest){
    this->destino = dest;
}
/**
 * Método que lê, de um ficheiro txt, o número de depósitos de uma linha.
 * 1º Linha - Depósitos Frescos
 * 2º Linha - Depósitos Normais
 * @param str
 * @return 
 */
int Ficheiro::lerDepositos(char *str, int length){
    int n = 0;
    for (int i = 0; i < length; i++){
        if ( n != 0) break;
        if ( str [i] == ':'){
            for (int j = (i+1); j < length ;j++){
                if ( str [j] ==  ';'){
                    break;
                }else{
                    n = (n*10) +  str[j] - '0';
                }
                
            }
        }
    }return n;
    
}
/**
 * Método que escreve informação do Armazém para ficheiro .txt
 * @param a
 */
void Ficheiro::escreverFicheiro(const Armazem& a){
  /*OFSTREAM PARA ESCRITA DE FICHEIROS*/
  ofstream ficheiro_txt;
  
    ficheiro_txt.open(output, ios::trunc );
    ficheiro_txt <<= a;
    ficheiro_txt.close();
}

/**
 * Método que lê informação do Armazém do ficheiro .txt
 * @param a
 */
void Ficheiro::lerFicheiro( Armazem& a ){
    /*IFSTREAM PARA LEITURA DE FICHEIROS*/
    ifstream ficheiro_txt(this->destino);
    int i = 0;
    vector < vector <int> > matrix;
   
    string wasted;
    while ( i < LINES ){
        
        string tmp; /*STRING*/
        getline(ficheiro_txt, tmp); 
        char str [MATRIX_LINE];  /*ARARY CHAR*/
        /*CONVERTER STRING*/
        strncpy(str, tmp.c_str(),sizeof(str));  
    
   
    int n = 0;
    n = lerDepositos(str,MATRIX_LINE);
    
    if ( a.getNumDepFrescos() != 0){
       a.setNumDepNormais(n);
       break;
      }
    a.setNumDepFrescos(n);
    i++;
    }
    getline(ficheiro_txt, wasted); 
    
    string temp;
    while( getline(ficheiro_txt,temp) ){/*LINHAS PARA LER*/
         vector < int > vec_aux;
        char str [MATRIX_LINE];  /*ARARY CHAR*/
        /*CONVERTER STRING*/
        strncpy(str, temp.c_str(),MATRIX_LINE);
        
        for (int i = 0; i < MATRIX_LINE; i= i + 2){
            int v = str[i] - '0';
            vec_aux.push_back(v);
       
        }
        string temp; /*STRING*/
        matrix.push_back(vec_aux);
    }
    
    a.setMatrizDistancia(matrix);
    
    ficheiro_txt.close();
      
}


#endif	/* FICHEIRO_H */

