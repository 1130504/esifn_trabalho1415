/* 
 * File:   Deposito.h
 * Author: Bruno Freitas
 *
 * Created on 20 de Outubro de 2014, 22:38
 */

#ifndef DEPOSITO_H
#define	DEPOSITO_H
#include <string>
#include "Produto.h" 
#include <iostream>

using namespace std;

class Deposito {
protected:
    int chave;
    int numPaletes;
    int capacidadePaletes;

public:

    Deposito();
    virtual ~Deposito();
    Deposito(int m_chave, int m_numPaletes, int m_capacidadePaletes);
    Deposito(const Deposito &d);

    virtual bool inserirProduto(const Produto &p) = 0;
    virtual Produto retirarProduto() = 0;
    virtual bool inserirNProdutos(vector <Produto>& vecProd) = 0;
    virtual vector<Produto> retirarNProdutos(int numProdutosRetirar) = 0;
    virtual bool podeEmpilhar() = 0;

    int getChave();
    int getNumPaletes();
    int getCapacidade();
    void setChave(int m_chave);
    void setNumPaletes(int m_numPaletes);
    void setCapacidadePaletes(int m_capacidade);
    virtual string getClassName() = 0;
    virtual void writeStream(ostream& out) const;

};

Deposito::Deposito() {
}

Deposito::~Deposito() {
}

Deposito::Deposito(int m_chave, int m_numPaletes, int m_capacidadePaletes) {
    chave = m_chave;
    numPaletes = m_numPaletes;
    capacidadePaletes = m_capacidadePaletes;
}

Deposito::Deposito(const Deposito& d) {
    chave = d.chave;
    numPaletes = d.numPaletes;
    capacidadePaletes = d.capacidadePaletes;
}

int Deposito::getChave() {
    return chave;
}

int Deposito::getNumPaletes() {
    return numPaletes;
}

int Deposito::getCapacidade() {
    return capacidadePaletes;
}


void Deposito::setChave(int m_chave) {
    chave = m_chave;
}

void Deposito::setCapacidadePaletes(int m_capacidade) {
    capacidadePaletes = m_capacidade;
}

void Deposito::setNumPaletes(int m_numPaletes) {
    numPaletes = m_numPaletes;
}


void Deposito::writeStream(ostream& out) const {
    out << "Número de Paletes: " << this ->numPaletes << endl;
    out << "Carga maxima de cada palete: " << this ->capacidadePaletes << endl;
    out << "ID: " << this->chave << endl;
}

ostream& operator<<(ostream& out, const Deposito& dep) {
    dep.writeStream(out);
    return out;
}
#endif	/* DEPOSITO_H */

