/* 
 * File:   DepositoFresco.h
 * Author: Bruno Freitas
 *
 * Created on 21 de Outubro de 2014, 18:42
 */

#ifndef DEPOSITOFRESCO_H
#define	DEPOSITOFRESCO_H

#include <queue>
#include <vector>
#include "Deposito.h"
#include <iostream>
using namespace std;

class DepositoFresco : public Deposito {
private:
    vector <queue <Produto> > vecDepositos;
    int nPaleteInserir;
    int nPaleteRemover;

public:
    DepositoFresco();
    ~DepositoFresco();
    DepositoFresco(int m_chave, int m_numPaletes, int m_capacidadePaletes);
    DepositoFresco(const DepositoFresco& df);
    Deposito* clone() const;

    void writeStream(ostream& out) const;
    string getClassName();
    bool podeEmpilhar();
    bool inserirProduto(const Produto& p);
    bool inserirNProdutos(vector<Produto>& vecProd);
    Produto retirarProduto();
    vector<Produto> retirarNProdutos(int numProdutosRetirar);

};

DepositoFresco::DepositoFresco() : Deposito() {

}

DepositoFresco::~DepositoFresco() {

}

DepositoFresco::DepositoFresco(int m_chave, int m_numPaletes, int m_capacidadePaletes) : Deposito(m_chave, m_numPaletes, m_capacidadePaletes), vecDepositos(m_numPaletes) {
    this->nPaleteInserir = 0;
    this->nPaleteRemover = 0;
}

DepositoFresco::DepositoFresco(const DepositoFresco& df) : Deposito(df), vecDepositos(df.vecDepositos) {
    this->nPaleteInserir = df.nPaleteInserir;
    this->nPaleteRemover = df.nPaleteRemover;
}

Deposito* DepositoFresco::clone() const {
    return new DepositoFresco(*this);
}

bool DepositoFresco::podeEmpilhar() {
    vector < queue <Produto> >::const_iterator it;
    for (it = this->vecDepositos.begin(); it != this->vecDepositos.end(); ++it) {
        if (it->size() < this->capacidadePaletes) {
            return true;
        }
    }
    return false;
}

bool DepositoFresco::inserirProduto(const Produto& p) {
    if (!this->podeEmpilhar()) {
        return false;
    }
    vecDepositos.at(nPaleteInserir).push(p);
    nPaleteInserir++;

    if (nPaleteInserir == this->numPaletes) {
        nPaleteInserir = 0;
    }
    return true;
}

bool DepositoFresco::inserirNProdutos(vector<Produto>& vecProd) {
    int index;
    int indexf = vecProd.size();
    bool flag;

    for (index = 0; index < indexf; index++) {
        if (inserirProduto(vecProd[index])) {
            flag = true;
        } else return flag = false;
    }
    return flag;
}

Produto DepositoFresco::retirarProduto() {
    Produto p;
    if (vecDepositos.size() == 0) {
        return p;
    }
    p = vecDepositos.at(nPaleteRemover).front();
    vecDepositos.at(nPaleteRemover).pop();
    nPaleteRemover++;

    if (nPaleteRemover == this->numPaletes) {
        nPaleteRemover = 0;
    }

    return p;
}

vector <Produto> DepositoFresco::retirarNProdutos(int numProdutosRetirar) {
    vector<Produto> vec;
    int index;
    for (index = 0; index < numProdutosRetirar; index++) {
        vec.push_back(retirarProduto());
    }
    return vec;
}

string DepositoFresco::getClassName() {

    return "Deposito Fresco";
}



ostream& operator<<(ostream& out, const DepositoFresco & dep) {
    dep.writeStream(out);
    return out;
}

void DepositoFresco::writeStream(ostream & out) const {
    vector < queue <Produto> >::const_iterator it;
    int nPaletes;
    out << "***************** Depósito Fresco *****************" << endl;
    Deposito::writeStream(out);
    out << endl;
    for (nPaletes = 0, it = this->vecDepositos.begin(); it != this->vecDepositos.end(); ++it, nPaletes++) {
        out << "Palete nº " << nPaletes + 1 << " ";
        out << "com " << it->size() << " Produto(s): ";
        out << endl;
//        for (queue<Produto> tmp = *it; !tmp.empty(); tmp.pop()) {
//            out << tmp.front();
//        }
        out << "____________________________" << endl;
    }
}
#endif	/* DEPOSITOFRESCO_H */

