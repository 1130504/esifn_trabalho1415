/* 
 * File:   main.cpp
 * Author: Bruno Freitas
 *
 * Created on 20 de Outubro de 2014, 22:22
 */

#include <cstdlib>
#include "Armazem.h"
#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "Produto.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "iostream"
#include "ArmazemGraph.h"

using namespace std;

int main(int argc, char** argv) {

    Armazem a;
    ArmazemGraph grafo;
    //    a.simularArmazem(5,1);
    //    int a;
    //    for (int i = 0; i < 11; i++) {
    //        matriz.resize(11);
    //        for (int j = 0; j < 11; j++) {
    //            matriz[i].resize(11);
    //        }
    //    } 
    //    for (int i = 0; i < 11; i++) {
    //        for (int j = 0; j < 11; j++) {
    //            cout << "v: ";
    //            cin >> a;
    //            cout << "\n";
    //            
    //            cout << a;
    //            matriz[i][j] = a;
    //        }
    //    }
    //    for (int i = 0; i < 11; i++) {
    //        for (int j = 0; j < 11; j++) {
    //            cout << matriz[i][j];
    //        }
    //    } 
    //    
    //        a.escreverFicheiro();
    //        cout << a;
    //    a.simularArmazem(10,1);
    //    escreverFicheiro(a);
    //    int id1 = 4;
    //    int id2 = 3;
    //    grafo.apresentarPercursos(id1, id2);
    //    grafo.calcularCaminhoMinimo(id1,id2);
    //    cout<< "\n";
    //    cout << grafo;
    //    
    //    

    vector<vector<int> > matrix;


    int choice;
    int id_dep1;
    int id_dep2;
    int i, t;
    do {
        cout << "Menu\n";
        cout << "Escolha a opçao desejada:\n";
        cout << "1 - Simular armazem e construir grafo\n";
        cout << "2 - Apresentar todos os percursos entre dois depósitos\n";
        cout << "3 - Apresentar um percurso entre dois depósitos(Fresco/Normal)\n";
        cout << "4 - Calcular percurso mais curto entre dois depósitos\n";
        cout << "5 - Sair\n";
        cout << "Opção: ";
        cin >> choice;

        switch (choice) {

            case 1:
                a.simularArmazem(15, 1);
                escreverFicheiro(a);
                matrix = lerFicheiro(grafo);
                cout <<= a;
                grafo.construirGrafoFromFile(matrix);
                cout << grafo;
                break;

            case 2:
                cout << "\nIntroduza o primeiro ID: ";
                cin >> id_dep1;
                cout << "\n";
                cout << "Introduza o segundo ID: ";
                cin >> id_dep2;
                if (id_dep1 != id_dep2) {
                    grafo.apresentarPercursos(id_dep1, id_dep2);
                }
                break;

            case 3:
                cout << "Goodbye!";
                break;
            case 4:
                cout << "\nIntroduza o primeiro ID: ";
                cin >> id_dep1;
                cout << "\n";
                cout << "Introduza o segundo ID: ";
                cin >> id_dep2;
                if (id_dep1 != id_dep2) {
                    grafo.calcularCaminhoMinimo(id_dep1, id_dep2);
                    cout << "\n";
                }
            case 5:
                break;
            default:
                cout << "Menu\n";
                cout << "Escolha a opçao desejada:\n";
                cout << "1 - Construir grafo\n";
                cout << "2 - Apresentar todos os percursos entre dois depósitos\n";
                cout << "3 - Apresentar um percurso entre dois depósitos(Fresco/Normal)\n";
                cout << "4 - Calcular percurso mais curto entre dois depósitos\n";
                cout << "5 - Sair\n";
                cout << "Opção: ";
                cin >> choice;
        }
    } while (choice != 5);

    return 0;
}





