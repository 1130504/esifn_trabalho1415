/* 
 * File:   ArmazemGraph.h
 * Author: Bruno Freitas
 *
 * Created on 2 de Dezembro de 2014, 16:24
 */

#ifndef ARMAZEMGRAPH_H
#define	ARMAZEMGRAPH_H
#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "Armazem.h"
#include "graphStlPath.h"
#include "vector"

const int LINES = 2;
const int MATRIX_LINE = 100;


using namespace std;

class ArmazemGraph : public graphStlPath<int, int> {
private:
    void mostraCaminho(int vi, const vector<int> &path);
    void menorCaminho(int &ci, int &cf);
    int numDepNormais;
    int numDepFrescos;
public:

    ArmazemGraph();
    ArmazemGraph(const ArmazemGraph& grafo);

    virtual ~ArmazemGraph() {
    }
    void construirGrafoFromFile(vector<vector<int > > matriz);
    void apresentarPercursos(int id_dep1, int id_dep2);
    void calcularCaminhoMinimo(int &id_dep1, int &id_dep2);
    void apresentarPercursoFresco(int id_dep1, int id_dep2);
    void apresentarPercursoNormal(int id_dep1, int id_dep2);
    void setNumDepFrescos(int num);
    void setNumDepNormais(int num);
    int getNumDepFrescos();
    int getNumDepNormais();
};

ArmazemGraph::ArmazemGraph() : graphStlPath<int, int>() {

}

ArmazemGraph::ArmazemGraph(const ArmazemGraph& grafo) : graphStlPath<int, int>(grafo) {
    this->numDepFrescos = grafo.numDepFrescos;
    this->numDepNormais = grafo.numDepNormais;
}

void ArmazemGraph::setNumDepFrescos(int num) {
    this->numDepFrescos = num;
}

void ArmazemGraph::setNumDepNormais(int num) {
    this->numDepNormais = num;
}

int ArmazemGraph::getNumDepFrescos() {
    return this->numDepFrescos;
}

int ArmazemGraph::getNumDepNormais() {
    return this->numDepNormais;
}

void ArmazemGraph::construirGrafoFromFile(vector<vector<int > > matriz) {
    //    matriz[0][0] = 0;
    //    matriz[0][1] = 3;
    //    matriz[0][2] = 3;
    //    matriz[0][3] = 1;
    //    matriz[0][4] = 1;
    //
    //    matriz[1][0] = 1;
    //    matriz[1][1] = 0;
    //    matriz[1][2] = 4;
    //    matriz[1][3] = 4;
    //    matriz[1][4] = 3;
    //
    //    matriz[2][0] = 3;
    //    matriz[2][1] = 4;
    //    matriz[2][2] = 0;
    //    matriz[2][3] = 4;
    //    matriz[2][4] = 4;
    //
    //    matriz[3][0] = 3;
    //    matriz[3][1] = 2;
    //    matriz[3][2] = 2;
    //    matriz[3][3] = 0;
    //    matriz[3][4] = 0;
    //
    //    matriz[4][0] = 0;
    //    matriz[4][1] = 2;
    //    matriz[4][2] = 2;
    //    matriz[4][3] = 0;
    //    matriz[4][4] = 0;

    int distancia;
    int tamanho_matriz = matriz.size();
    for (int linha = 0; linha < tamanho_matriz; linha++) {
        for (int coluna = 0; coluna < tamanho_matriz; coluna++) {
            if (linha != coluna && matriz[linha][coluna] != 0) {
                distancia = matriz[linha][coluna];
                this->addGraphEdge(distancia, linha, coluna);
            }
        }
    }
}

void ArmazemGraph::apresentarPercursos(int id_dep1, int id_dep2) {
    cout << "aqui";
    queue < stack <int> > qr;
    qr = distinctPaths(id_dep1, id_dep2);
    if (qr.empty()) {
        cout << "vazio";
    }
    int tamanho;
    while (!qr.empty()) {
        tamanho = qr.front().size();
        for (int i = 0; i < tamanho; i++) {
            cout << qr.front().top();
            qr.front().pop();
        }
        cout << "\n";
        qr.pop();
    }
}

void ArmazemGraph::calcularCaminhoMinimo(int& id_dep1, int& id_dep2) {
    cout << "Menor distância entre Depósito nº" << id_dep1 << " e Depósito nº " << id_dep2 << ": ";
    menorCaminho(id_dep1, id_dep2);
}

void ArmazemGraph::apresentarPercursoFresco(int id_dep1, int id_dep2) {
    if (id_dep1 <= numDepFrescos && id_dep2 <= numDepFrescos) {
            
    } else {
        cout << "\nO ID inserido é de um depósito normal!";
    }
}

void ArmazemGraph::apresentarPercursoNormal(int id_dep1, int id_dep2) {
    if (id_dep1 > numDepFrescos && id_dep2 > numDepFrescos) {

    } else {
        cout << "\nO ID inserido é de um depósito Fresco!";
    }
}

void ArmazemGraph::menorCaminho(int &id_dep1, int &id_dep2) {
    vector <int> path;
    vector<int> dist;
    int key;

    this->getVertexKeyByContent(key, id_dep2);
    this->dijkstrasAlgorithm(id_dep1, path, dist);
    cout << dist[key] << " Ramos ; " << endl;
    cout << id_dep1;
    mostraCaminho(key, path);
}

void ArmazemGraph::mostraCaminho(int vi, const vector<int> &path) {
    if (path[vi] == -1) {
        return;
    }
    mostraCaminho(path[vi], path);
    int e;
    this->getEdgeByVertexKeys(e, path[vi], vi);

    int c;
    this->getVertexContentByKey(c, vi);
    cout << " -> " << e << " -> " << c;
}

/**
 * Método que lê, de um ficheiro txt, o número de depósitos de uma linha.
 * 1º Linha - Depósitos Frescos
 * 2º Linha - Depósitos Normais
 * @param str
 * @return 
 */
int lerDepositos(char *str, int length) {
    int n = 0;
    for (int i = 0; i < length; i++) {
        if (n != 0) break;
        if (str [i] == ':') {
            for (int j = (i + 1); j < length; j++) {
                if (str [j] == ';') {
                    break;
                } else {
                    n = (n * 10) + str[j] - '0';
                }

            }
        }
    }
    return n;
}

vector<vector<int > > lerFicheiro(ArmazemGraph &a) {
    /*IFSTREAM PARA LEITURA DE FICHEIROS*/
    ifstream ficheiro_txt("dados.txt");



    /*DEPOSITOS FRESCOS*/
    string tmp; /*STRING*/
    getline(ficheiro_txt, tmp);
    char str [MATRIX_LINE]; /*ARARY CHAR*/
    /*CONVERTER STRING*/
    strncpy(str, tmp.c_str(), sizeof (str));


    int n = 0;
    n = lerDepositos(str, MATRIX_LINE);
    a.setNumDepFrescos(n);

    /*DEPOSITOS NORMAIS*/
    string tmp_1; /*STRING*/
    getline(ficheiro_txt, tmp_1);
    char str_1 [MATRIX_LINE]; /*ARARY CHAR*/
    /*CONVERTER STRING*/
    strncpy(str_1, tmp.c_str(), sizeof (str_1));

    n = 0;
    n = lerDepositos(str_1, MATRIX_LINE);
    a.setNumDepNormais(n);



    vector < vector <int> > matrix;
    string temp;
    int p = 0, v = 0;
    while (getline(ficheiro_txt, temp)) {/*LINHAS PARA LER*/
        vector < int > vec_aux;
        char str2 [MATRIX_LINE]; /*ARARY CHAR*/
        /*CONVERTER STRING*/
        strncpy(str2, temp.c_str(), MATRIX_LINE);

        for (int i = 0; i < MATRIX_LINE; i++) {
            if (str2[i] != ' ') {
                if (str2[i + 1] != ' ') {
                    p = str2[i] - '0';
                    v = str2[i + 1] - '0';
                    v = p * 10 + v;
                    vec_aux.push_back(v);
                    i++;
                } else {
                    int v = str2[i] - '0';
                    vec_aux.push_back(v);
                }
            }
        }
        string temp; /*STRING*/
        matrix.push_back(vec_aux);
    }

    return matrix;

    ficheiro_txt.close();

}
#endif	/* ARMAZEMGRAPH_H */

