/* 
 * File:   Armazem.h
 * Author: Bruno Freitas
 *
 * Created on 21 de Outubro de 2014, 16:19
 */

#ifndef ARMAZEM_H
#define	ARMAZEM_H
#include <vector>
#include <typeinfo>
#include <string>
#include <ctime>
#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
using namespace std;

class Armazem {
    vector <Deposito*> depositos;
    int numDepNormais;
    int numDepFrescos;
    vector<vector<int> > matrizDistancias;

public:
    Armazem();
    ~Armazem();
    Armazem(const Armazem& armzm);
    void simularArmazem(int limiteS, int limiteI);

    int getNumDepNormais() {
        return numDepNormais;
    }

    int getNumDepFrescos() {
        return numDepFrescos;
    }

    void setNumDepFrescos(int df) {
        this->numDepFrescos = df;
    }

    void setNumDepNormais(int dn) {
        this->numDepNormais = dn;
    }
    int lerDepositos(char *str, int lenght);
    void lerFicheiro(Armazem &a);
    vector<vector<int> > getMatrizDistancia() const;
    void setMatrizDistancia(vector < vector <int> > matrix);
    void inserirDeposito(Deposito& novo_deposito);
    void retirarDeposito(int chave);
    void escreverMatriz(ostream& out) const;
    void escreverFicheiro(const Armazem &a);
    void listar(ostream& out) const;
    string stringFicheiro();
    int randomNumberLimit(int limiteS, int limiteI);
};

Armazem::Armazem() : depositos(), matrizDistancias() {
}

Armazem::~Armazem() {
}

Armazem::Armazem(const Armazem& armzm) : depositos(armzm.depositos), matrizDistancias(armzm.matrizDistancias) {
    numDepFrescos = armzm.numDepFrescos;
    numDepNormais = armzm.numDepNormais;
}

int Armazem::randomNumberLimit(int limiteS, int limiteI) {
    int random_integer = rand() % limiteS + limiteI;
    return random_integer;
}

void Armazem::simularArmazem(int limiteS, int limiteI) {
    srand((unsigned) time(NULL));
    int x, y;
    this->depositos.clear();
    this->numDepFrescos = randomNumberLimit(limiteS, limiteI);
    int produto = 0;
    for (x = 0; x < numDepFrescos; x++) {
        int id = x;
        int n_palete = randomNumberLimit(limiteS, limiteI);
        int cap = randomNumberLimit(limiteS, limiteI);
        DepositoFresco *d = new DepositoFresco(id, n_palete, cap);
        depositos.push_back(d);
        produto = randomNumberLimit(1, 20);
        for (int z = 0; z < produto; z++) {
            int ID_P = randomNumberLimit(limiteS, limiteI);
            Produto pz(ID_P, "sem descrição");
            d->inserirProduto(pz);
        }
    }
    this->numDepNormais = randomNumberLimit(limiteS, limiteI);
    for (y = 0; y < numDepNormais; y++) {
        int id = x + y;
        int n_palete = randomNumberLimit(limiteS, limiteI);
        int cap = randomNumberLimit(limiteS, limiteI);
        DepositoNormal *d;
        if (y / 2 == 0) {
            d = new DepositoNormal(id, n_palete, cap);
            depositos.push_back(d);
        } else {
            d = new DepositoNormal(id, n_palete, cap / 2);
            depositos.push_back(d);
        }

        produto = randomNumberLimit(1, 20);
        for (int w = 0; w < produto; w++) {
            int ID_P = randomNumberLimit(limiteS, limiteI);
            Produto pz(ID_P, "sem descrição");
            d->inserirProduto(pz);
        }
    }

    int tamanhoMatriz = this->numDepFrescos + this->numDepNormais;
    this->matrizDistancias.resize(tamanhoMatriz);
    for (int s = 0; s < tamanhoMatriz; s++) {
        this->matrizDistancias[s].resize(tamanhoMatriz);
    }
    for (int linha = 0; linha < tamanhoMatriz; ++linha) {
        for (int coluna = 0; coluna < tamanhoMatriz; ++coluna) {
            if (linha == coluna) {
                this->matrizDistancias[linha][coluna] = 0;
            } else {
                this->matrizDistancias[linha][coluna] = randomNumberLimit(limiteS, 0);
            }
        }
    }
}

vector<vector<int> > Armazem::getMatrizDistancia() const {
    return this->matrizDistancias;
}

void Armazem::setMatrizDistancia(vector < vector <int> > matrix) {
    this->matrizDistancias = matrix;

}

void Armazem::inserirDeposito(Deposito & novo_deposito) {
    depositos.push_back(&novo_deposito);
}

void Armazem::retirarDeposito(int chave) {
    vector <Deposito*>::iterator it;
    for (it = depositos.begin(); it != depositos.end();) {
        if ((*it)->getChave() == chave) {
            it = depositos.erase(it);
        } else {

            it++;
        }
    }
}

void Armazem::listar(ostream & out)const {
    vector<Deposito*>::const_iterator it;
    out << "*****Armazem******" << endl;
    out << "\nNúmero de depósitos: " << depositos.size() << endl;
    for (it = depositos.begin(); it != depositos.end(); ++it) {
        out << "\n" << (**it);
    }
    int t = this->matrizDistancias.size();
    for (int i = 0; i < t; i++) {
        for (int x = 0; x < t; x++) {
            cout << this->matrizDistancias[i][x];
            cout << "   ";
        }
        cout << "\n";
    }
}

/**
 * Escreve a matriz num ficheiro txt juntamento com o número total de depósitos, frescos e normais.
 * @param out
 */
void Armazem::escreverMatriz(ostream & out) const {
    ostringstream dep;
    string dep_aux;
    dep << "Frescos:" << this->numDepFrescos << ";\n" << "Normais:" << this->numDepNormais << ";\n";
    dep_aux = dep.str();
    out << dep_aux;


    string matriz;
    int t = this->matrizDistancias.size();
    for (int i = 0; i < t; i++) {
        ostringstream matrix;
        for (int x = 0; x < t; x++) {
            matrix << this->matrizDistancias[i][x] << " ";
            matriz = matrix.str();
        }
        out << matriz << "\n";
    }
}

ostream& operator<<(ostream& out, const Armazem & a) {
    a.listar(out);
    return out;
}

ostream& operator<<=(ostream& out, const Armazem & a) {
    a.escreverMatriz(out);
    return out;
}

void escreverFicheiro(const Armazem & a) {
    /*OFSTREAM PARA ESCRITA DE FICHEIROS*/
    ofstream ficheiro_txt;

    ficheiro_txt.open("dados.txt", ios::trunc);
    ficheiro_txt <<= a;
    ficheiro_txt.close();
}


#endif	/* ARMAZEM_H */

