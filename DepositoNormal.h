/* 
 * File:   DepositoFresco.h
 * Author: Pedro Gomes
 *
 * Created on 21 de Outubro de 2014, 18:42
 */

#ifndef DEPOSITONORMAL_H
#define	DEPOSITONORMAL_H
#include <vector>
#include <stack>
#include <iostream>
#include "Deposito.h"
#include "DepositoFresco.h"


using namespace std;

class DepositoNormal : public Deposito {
private:

    vector<stack <Produto> > paletes;



public:


    DepositoNormal();
    ~DepositoNormal();
    DepositoNormal(int m_chave, int m_numPaletes, int m_capacidadePaletes);
    DepositoNormal(const DepositoNormal &dep_normal);
    Deposito* clone()const;

    bool inserirProduto(const Produto& p);
    Produto retirarProduto();
    bool inserirNProdutos(vector<Produto>& vecProd);
    vector<Produto> retirarNProdutos(int numProdutosRetirar);
    bool podeEmpilhar();
    void writeStream(ostream& out) const;
    string getClassName();
    bool valida_pares(stack <Produto> palete);
    bool valida_impares(stack <Produto> palete);
};

DepositoNormal::DepositoNormal() : Deposito() {

}

DepositoNormal::~DepositoNormal() {

}

DepositoNormal::DepositoNormal(int m_chave, int m_numPaletes, int m_capacidadePaletes) : Deposito(m_chave, m_numPaletes, m_capacidadePaletes), paletes(m_numPaletes) {

}

DepositoNormal::DepositoNormal(const DepositoNormal &dep_normal) : Deposito(dep_normal), paletes(dep_normal.paletes) {

}

Deposito* DepositoNormal::clone() const {
    return new DepositoNormal(*this);
}

bool DepositoNormal::valida_pares(stack <Produto> palete) {

    if (palete.size() < this->capacidadePaletes) {
        return true;
    }
    return false;

}

bool DepositoNormal::valida_impares(stack <Produto> palete) {
    if (palete.size() < this->capacidadePaletes / 2) {
        return true;
    }
    return false;

}

bool DepositoNormal::inserirProduto(const Produto& p) {

    for (int i = 0; i < this->numPaletes; i = i + 2) {
        if (valida_pares(this->paletes.at(i))) {

            this->paletes.at(i).push(p);
            return true;

        }
    }
    for (int i = 1; i< this->numPaletes; i = i + 2) {
        if (valida_impares(this->paletes.at(i))) {
            this->paletes.at(i).push(p);
            return true;

        }
    }
    return false;
}

Produto DepositoNormal::retirarProduto() {
    Produto p;
    for (int i = 1; i < this->numPaletes; i = i + 2) {
        while (!this->paletes.at(i).empty()) {
            p = this->paletes.at(i).top();
            this->paletes.at(i).pop();
            return p;
            break;
        }
    }
    for (int i = 0; i< this->numPaletes; i = i + 2) {
        while (!this->paletes.at(i).empty()) {
            p = this->paletes.at(i).top();
            this->paletes.at(i).pop();
            return p;
            break;
        }
    }
}

bool DepositoNormal::inserirNProdutos(vector<Produto>& vecProd) {
    bool flag;


    for (int i = 0; i < vecProd.size(); i++) {
        Produto p = vecProd.at(i);
        if (inserirProduto(p)) {
            flag = true;
        } else return false;


    }
    return flag;

}

vector<Produto> DepositoNormal::retirarNProdutos(int numProdutosRetirar) {
    vector<Produto> vec;
    int index;
    for (index = 0; index < numProdutosRetirar; index++) {
        vec.push_back(retirarProduto());
    }
    return vec;
}

string DepositoNormal::getClassName() {
    return "Deposito Normal";
}

ostream& operator<<(ostream& out, const DepositoNormal & dep_normal) {
    dep_normal.writeStream(out);

    return out;
}

void DepositoNormal::writeStream(ostream & out) const {
    vector < stack <Produto> >::const_iterator it;
    int nPaletes;
    out << "***************** Depósito Normal *****************" << endl;
    Deposito::writeStream(out);
    out << endl;
    for (nPaletes = 0, it = this->paletes.begin(); it != this->paletes.end(); ++it, nPaletes++) {
        out << "Palete nº " << nPaletes << " ";
        out << "com " << it->size() << " Produto(s)";
        out << endl;
//        for (stack<Produto> tmp = *it; !tmp.empty(); tmp.pop()) {
//            out << tmp.top() << endl;
//        }
        out << "____________________________" << endl;
    }
}

bool DepositoNormal::podeEmpilhar() {
    return false;
}

#endif	/* DEPOSITOFRESCO_H */