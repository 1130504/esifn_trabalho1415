/* 
 * File:   Produto.h
 * Author: Bruno Freitas
 *
 * Created on 20 de Outubro de 2014, 22:25
 */

#ifndef PRODUTO_H
#define	PRODUTO_H
#include <string>
#include <iostream>

using namespace std;

class Produto {
    
    int codProduto;
    string descricao;

public:

    Produto() {
    };

    ~Produto() {
    };

    Produto(int m_codProduto, string m_descricao) {
        codProduto = m_codProduto;
        descricao = m_descricao;
    }

    Produto(const Produto &prod) {
        codProduto = prod.codProduto;
        descricao = prod.descricao;
    }

    void writeStream(ostream& out) const;
    int getCodigo();
    string getDescricao();
    void setCodigo(int novo_codigo);
    void setDescricao(string nova_descricao);
};

int Produto:: getCodigo(){
    return codProduto;
}
string Produto::getDescricao(){
    return descricao;
}

void Produto::setCodigo(int novo_codigo){
    codProduto = novo_codigo;
}
void Produto::setDescricao(string nova_descricao){
    descricao = nova_descricao;
}

ostream& operator<<(ostream& out, const Produto& p){
    p.writeStream(out);
    return out;
}

void Produto::writeStream(ostream& out) const{
    out << "\tCódigo do produto: " << this->codProduto <<endl;
    out << "\tDescição do produto: " << this->descricao << endl;
}


#endif	/* PRODUTO_H */